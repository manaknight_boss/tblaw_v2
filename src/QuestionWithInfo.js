import React, { Component } from 'react';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithInfo extends Component {

    constructor(props) {
        super(props)
        //const { handle } = this.props.match.params
        this.state = {
            showGuidanceModal: false
        }
    }

    toggleInfoModal(visibility) {
        this.setState({ showGuidanceModal: visibility });
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <button className="btn btn-primary" onClick={this.props.onClick}>Next</button>{' '}
                <button className="btn btn-info" onClick={() => this.toggleInfoModal(true)}>Info</button>
                {this.state.showGuidanceModal ? <GuidanceModal body={this.props.info} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}
