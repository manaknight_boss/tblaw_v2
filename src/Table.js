import React, { Component } from 'react';
import TableModal from './TableModal';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithSplitTable extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            userData: [],
            showTableModal: false,
            currentRow: -1,
            currentCol: -1,
            currentData: [],
            complete: false
        }
    }

    componentDidMount() {
        var userInput = this.props.userData;
        var t = JSON.stringify(this.state.data);
            var userData = this.state.userData;
            for (let i = 0; i < userInput.length; i++) {
                var data = JSON.parse(t);
                for (let j = 0; j < data.length; j++) {
                    if(userInput[i][data[j].name] == null) {
                        data[j].value = '';
                    } else {
                        data[j].value = userInput[i][data[j].name];
                    }
                    
                }
                userData.push(data);
            }
    }

    toggleTableModal(visibility) {
        this.setState({ showTableModal: visibility });
    }

    submit(response) {
        console.log("submit");
        console.log(response);
        this.toggleTableModal(false);
        var userData = this.state.userData;
        var cell = userData[this.state.currentRow][this.state.currentCol];
        cell.value = response[0].value;
        this.checkIfComplete();
    }

    checkIfComplete() {
        var userData = this.state.userData;
        for(let i = 0;i < userData.length; i++) {
            for(let j = 0; j < userData[i].length; j++) {
                if(userData[i][j].value === '') {
                    return;
                }
            }
        }
        this.setState({complete: true});
    }

    removeValue(row, col) {
        var userData = this.state.userData;
        var cell = userData[row][col];
        cell.value = '';
        this.setState({complete: false});
    }

    addValue(row, col) {
        var userData = this.state.userData;
        this.setState({currentRow: row});
        this.setState({currentCol: col});
        var currentData = this.state.currentData;
        currentData = [];
        currentData.push(userData[row][col]);
        this.setState({currentData: currentData});
        this.toggleTableModal(true);
    }

    saveUserData() {
        var userData = this.state.userData;
        var processedUserData = [];
        console.log(userData);
        for(let i = 0; i < userData.length; i++) {
            var row = {};
            for(let j = 0; j < userData[i].length; j++) {
                var key = userData[i][j].name;
                var value = userData[i][j].value;
                var type = userData[i][j].type;
                if(type === 'number'){
                    row[key] = Number(value);
                } else {
                    row[key] = value;
                }
                
            }
            processedUserData.push(row);
        }
        this.props.onClick(processedUserData)
    }

    render() {
        return (
            <div>
                <div>
                    <h1>{this.props.title}</h1>
                    <br />
                    <span><button className="btn btn-sm btn-primary" onClick={() => null}>Add</button></span>
                    <br />
                    <div className="table-responsive">
                        <table className="table table-condensed table-bordered table-responsive table-hover table-striped">
                            <thead>
                                <tr>
                                    {this.state.data.map((value, index) => {
                                        return (<th key={index}>{value.name}</th>)
                                    })}
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.userData.map((row, i) => {
                                            return (<tr key={i}>
                                                {row.map((col, j) => {
                                                return (<td key={j}>{col.value === '' ? <span><button className="btn btn-sm btn-primary" onClick={() => this.addValue(i,j)}>Add {col.name}</button></span> : col.value}
                                                {col.value !== '' && col.state === "new" ?
                                                <span>{' '}<button className="btn btn-sm btn-primary" onClick={() => this.removeValue(i, j)}>Remove {col.name}</button></span>
                                                : null}</td>)
                                                })}
                                                
                                                {/* <td><button className="btn btn-sm btn-primary" onClick={() => this.removeItem(i)}>Remove</button></td> */}
                                            </tr>)
                                        })}
                            </tbody>
                        </table>
                    </div>
                    <button className="btn btn-primary" disabled={!this.state.complete} onClick={() => this.saveUserData()}>Next</button>
                </div>
                {/* <div>
                    Sorry, you can't answer this question until you fill in the question before question.
                    </div> */}
                    {this.state.showTableModal ? <TableModal data={this.state.currentData} visibility={this.state.showTableModal} onSubmit={(response) => this.submit(response)} onCancel={() => this.toggleTableModal(false)} /> : null}
                    {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}
