import React, { Component } from 'react';

export default class StartQuestionaire extends Component {

    constructor(props) {
        super(props)
        //const { handle } = this.props.match.params
        this.state = {
        }
    }

    //function which is called the first time the component loads
    componentDidMount() {

    }

    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <p>{this.props.body}</p>
                <button className="btn btn-primary" onClick={this.props.onClick}>Start Questionaire</button>
            </div>
        )
    }
}
