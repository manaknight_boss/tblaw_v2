let constants = {
    baseUrl: 'http://localhost:9000/v1/api/submission/',
    endpoint:{
        NEW: 'new/',
        SAVE: 'save/'
    },
    TYPE_ONE_BUTTON: 1,
    TYPE_INFO_BUTTON: 2,
    TYPE_BLANK: 3,
    TYPE_TABLE: 4,
    TYPE_MIX_TABLE: 5,
    TYPE_NONE: 0,
    TYPE_MULTIPLE: 6,
    TYPE_BUTTONS: 7,
    TYPE_SPLIT_TABLE: 8,

    BUTTON: 'button',
    GUIDANCE: 'guidance'
}

export default constants