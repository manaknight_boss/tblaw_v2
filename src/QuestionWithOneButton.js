import React, { Component } from 'react';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithOneButton extends Component {

    constructor(props) {
        super(props)
        //const { handle } = this.props.match.params
        this.state = {
            guidanceModalBody: ''
        }
    }

    componentDidMount() {
        if(!this.props.questionState.info && this.props.question.guidance !== '') {
            this.toggleInfoModal(true, this.props.question.guidance);
        }
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <button className="btn btn-primary" onClick={this.props.onClick}>Next</button>
                {' '}{this.props.questionState.info? <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, this.props.question.guidance)}>Info</button> : null}
                {' '}{this.props.skippable? <button className="btn btn-primary" onClick={this.props.onSkip}>Skip</button> : null}
                {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}