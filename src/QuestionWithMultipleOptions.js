import React, { Component } from 'react';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithMultipleOptions extends Component {

    constructor(props) {
        super(props)
        //const { handle } = this.props.match.params
        this.state = {
            data: this.props.data,
            options: [],
            question: this.props.question,
            showGuidanceModal: false,
            guidanceModalBody: '',
            isSelected: false,
            userData: this.props.selected,
            isOtherRequired: this.props.otherRequired,
            isOtherSelected: false,
            isMultiAnswer: this.props.multiAnswer, 
            other: ''
        }
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    componentDidMount() {
        var data = this.state.data
        var userData = this.state.userData
        var options = [];
        //console.log("options", options);
        for (let i = 0; i < data.length; i++) {
            if (data[i].question_id === this.state.question.id) {
                var option = {
                    option: data[i].name,
                    guidance: data[i].guidance === "" ? false : data[i].guidance,
                    isOther: false,
                    isChecked: false,
                    inputType: this.state.isMultiAnswer? 'checkbox':'radio',
                    next: "Q0"
                };
                options.push(option);
            }
        }

        if(this.state.isOtherRequired) {
            let option = {
                option: '',
                guidance: false,
                isOther: true,
                isChecked: false,
                inputType: 'checkbox',
                next: "Q0"
            };
            options.push(option);
        }

        for (let i = 0; i < options.length; i++) {
            var isChecked = false;
            if(userData !== '') {
                console.log("userData", userData);
                isChecked = userData[i].isChecked;
            }
            options[i].isChecked = isChecked;
            if(options[i].isOther && options[i].isChecked) {
                this.setState({isOtherSelected: true});
                options[i].option = userData[i].option;
                //console.log("data", options[i].option);
                this.setState({other: options[i].option});
            }
            if(options[i].isChecked) {
                this.setState({isSelected: true});
            }
        }
        this.setState({ options: options });
        if(!this.props.questionState.info && this.props.question.guidance !== '') {
            this.toggleInfoModal(true, this.props.question.guidance);
        }
    }

    next() {
        this.props.onClick(this.state.options);
    }

    onTextChange = (e) => {
        this.setState({other: e.currentTarget.value}, () => this.updateOtherOptionText());
        
        //self.checkIfAnyOptionSelected();
    }

    updateOtherOptionText() {
        const options = this.state.options;
        for(let i = 0; i < options.length; i++) {
            if(options[i].isOther) {
                if(this.state.other !== '') {
                    options[i].isChecked = true;
                } else {
                    options[i].isChecked = false;
                }
                options[i].option = this.state.other;
                break;
            }
        }
        
        this.setState({options: options}, () => this.checkIfAnyOptionSelected());
    }

    onChange = (e) => {
        const options = this.state.options;
        if(this.state.isMultiAnswer) {
            for(let i = 0; i < options.length; i++) {
                if(options[i].option === e.currentTarget.value) {
                    options[i].isChecked = !options[i].isChecked;   
                }
            }
        } else {
            // console.log("change");
            for(let i = 0; i < options.length; i++) {
                if(options[i].option === e.currentTarget.value) {
                    options[i].isChecked = true;
                    console.log("change",true);
                } else {
                    options[i].isChecked = false;
                    console.log("change", false);
                }
            }
        }
        this.setState({options: options}, () => this.checkIfAnyOptionSelected());
    }

    checkIfAnyOptionSelected() {
        const options = this.state.options;
        var isChecked = false;
        for(let i = 0; i < options.length; i++) {
            if(options[i].isChecked) {
                isChecked = true;
                break;
            }
        }
        if(this.state.isOtherSelected) {
            if(this.state.other.trim() === '') {
                isChecked = false;
            } else {
                isChecked = true;
            }
            
        }

        this.setState({isSelected: isChecked});
    }

    toggleOtherTextArea = (e) => {
        this.setState(prevState => {
            return { isOtherSelected: !prevState.isOtherSelected }
        }, this.emptyTextArea())
        
    }

    emptyTextArea() {
        console.log('emptyTextArea');
        this.setState({other: ''}, () => this.updateOtherOptionText());
    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <div className="toolbar">
                    <ol type="A">
                        {this.state.options.map((value, index) => {
                            
                                return (<li key={index}>
                                    {!value.isOther ?
                                    <label style={{fontWeight: 'normal'}}>
                                        {/* <input ng-if="option.inputType=='checkbox'" type="checkbox" ng-model="multipleAnswers[state][$index].isChecked" value="{{multipleAnswers[state][$index].isChecked}}" ng-change="validateMultipleAnswers()" />  */}
                                        <input type={this.state.isMultiAnswer? 'checkbox':'radio'} onChange={this.onChange} value={value.option} checked={value.isChecked}/>
                                        {' '}{value.option}
                                        {' '}{value.guidance !== false ? <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, value.guidance)}><i className="glyphicon glyphicon-info-sign"></i></button> : null}
                                        
                                    </label> : <label style={{fontWeight: 'normal'}}>
                                        <input type="checkbox" onChange={this.toggleOtherTextArea} checked={this.state.isOtherSelected}/>
                                        {' '}Other:
                                    </label>
                                    }
                                </li>)
                        })}
                        {this.state.isOtherSelected ?
                                    <textarea rows="5" className="form-control" style={{maxWidth: '670px'}} onChange={this.onTextChange} value={this.state.other}></textarea> : null}
                    </ol>
                </div>
                <button className="btn btn-primary" disabled={!this.state.isSelected} onClick={()=> this.next()}>Next</button>
                {' '}{this.props.questionState.info? <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, this.props.question.guidance)}>Info</button> : null}
                {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}

