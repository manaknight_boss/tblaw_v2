import React, { Component } from 'react';
import constants from './asserts/constants';

export default class ShowResult extends Component {

    constructor(props) {
        super(props)
        var t = JSON.stringify(this.props.result);
        this.state = {
            result:JSON.parse(t)
        }
        console.log(this.state.result);
    }

    //function which is called the first time the component loads
    componentDidMount() {
        var result = this.state.result;
        result.question.splice(0, 0, {'name':'Start'});
        this.setState({result: result});
    }

    getQuestionType(question) {
        const currentState = this.state.result.states[question.flowchart_name];
        console.log("state", currentState);
        if(currentState != null) {
            if (currentState.info) {
                return constants.TYPE_INFO_BUTTON;
            } else if (currentState.blank && !currentState.multiple) {
                return constants.TYPE_BLANK;
            } else if (currentState.multiple && currentState.haveOptions) {
                return constants.TYPE_MULTIPLE;
            } else if (currentState.type === 1) {
                return constants.TYPE_TABLE;
            } else if (currentState.type === 2) {
                return constants.TYPE_MIX_TABLE;
            } else {
                return constants.TYPE_ONE_BUTTON;
            }
        } else {
            return constants.TYPE_NONE
        }
        
    }

    render() {
        return (
                <div>
                    <h1>Result</h1>
                    <br/>
                    {this.state.result.question.map((value, index) => {
                        const type = this.getQuestionType(value);
                        console.log("type"+type);
                        return (
                    <div key={index}>
                        <h3>{value.name}</h3>
                        {value.guidance != null && value.guidance !== "" ? <p><i>Info: {value.guidance}</i></p> : null}
                        {type === constants.TYPE_BLANK || type === constants.TYPE_ONE_BUTTON ? <p>{value.value}</p> : null}
                        {type === constants.TYPE_MULTIPLE ? 
                        <div>
                             <ol type="A" >
                             {value.value.map((value, index) => {
                            
                            return (<li key={index}>
                                 {!value.isOther ?
                                     <label style={{fontWeight: 'normal'}}>
                                         {value.isChecked ? <span className="glyphicon glyphicon-check"></span> : <span className="glyphicon glyphicon-unchecked"></span>}
                                         {' '}{value.option}
                                     </label> :
                                     <label style={{fontWeight: 'normal', verticalAlign: 'top'}}>
                                         {value.isChecked ? <span className="glyphicon glyphicon-check"></span> : <span className="glyphicon glyphicon-unchecked"></span>}
                                         {' '}Other:
                                         {value.option !== '' ? <div className="form-control" style={{width: '100%', height: 'auto', maxWidth: '670px'}}>
                                             {value.option}
                                         </div> : null}
                                         
                                 </label> }
                                 </li>)
                             })}
                             </ol>
                         </div> 
                         : null}
                         {type === constants.TYPE_TABLE || type === constants.TYPE_MIX_TABLE?
                                <div className="table-responsive">
                                    <table className="table table-condensed table-bordered table-responsive table-hover table-striped">
                                        <thead>
                                            <tr>
                                            {Object.keys(value.value[0]).map(function (key) {
                                                // var item = this.state.items[key]
                                            return (<th key={key} className="text-capitalize">{key}</th>)
                                        })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {value.value.map((row, i) => {
                                            return (<tr key={i}>
                                                {Object.keys(row).map(function (key) {
                                                    var item = row[key]
                                                    return (<td key={key}><span>{item}</span></td>)
                                                })}
                                                
                                            </tr>)
                                        })}
                                            
                                               
                                                    {/* <span ng-if="answerValueData[questionData.name]">
                                                        <span ng-if="questionData.name=='yes' && answerValueData[questionData.name]==outerIndex" class="label label-primary">Selected</span>
                                                    <span ng-if="questionData.name!='yes'">{{answerValueData[questionData.name]}}</span>
                                                    </span> */}
                                               
                                            
                                        </tbody>
                                    </table>
                                </div>
                            : null}

                        {index < this.state.result.question.length - 1 ? <hr />: <br />}
                    </div>)
                    })}
                </div>


            

                        

                     
                
                
        )
    }
}
