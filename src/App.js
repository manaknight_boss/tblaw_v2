import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Questionaire from './Questionaire'
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';


class App extends Component {
  render() {
    return (

      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
          <Switch>
                <Route exact path= "/" render={() => (
                  <Redirect to="/questionaire"/>
                )}/>
                 <Route exact path='/questionaire/:handle' component={Questionaire} />
          </Switch>
      </div>
    </Router>
    );
  }
}
export default App;