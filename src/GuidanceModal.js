import React, { Component } from 'react';
var Modal = require('react-bootstrap-modal')

export default class GuidanceModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: true
        }
    }

    componentDidMount() {
        this.setState({ open: true });
    }

    render() {
        let closeModal = () => this.setState({ open: false })
        return (
            <div>
                <Modal
                    show={this.props.visibility}
                    onHide={closeModal}
                    aria-labelledby="ModalHeader">
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>GUIDANCE</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>{this.props.body}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className='btn btn-primary' onClick={this.props.onClick}>Ok</button>
                    </Modal.Footer>
                </Modal>
            </div>

        )
    }
}
