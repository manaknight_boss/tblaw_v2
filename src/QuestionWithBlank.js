import React, { Component } from 'react';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithBlank extends Component {

    constructor(props) {
        super(props)
        //const { handle } = this.props.match.params
        this.state = {
            text: this.props.text
        }
        console.log("test", this.state.text);
    }

    handleChange = (event) => {
        this.setState({ text: event.target.value });
        this.props.onChange(event.target.value);
    }

    componentDidMount() {
        console.log("test2", this.state.text);
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <textarea className="blank-textarea w-100 form-control" rows="10" onChange={this.handleChange} value={this.state.text}></textarea>
                <br />
                <br />
                <button className="btn btn-primary" disabled={!this.state.text} onClick={this.props.onClick}>Next</button>
                {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}

