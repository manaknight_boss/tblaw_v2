import React, { Component } from 'react';
import TableModal from './TableModal';
import GuidanceModal from './GuidanceModal';
import constants from './asserts/constants';

export default class QuestionWithTable extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            userData:[],
            showTableModal: false,
            guidanceModalBody: '',
            options: []
        }
        console.log("reload");
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    componentDidMount() {
        var selected = this.props.selected;
        if(selected !== '') {
            var t = JSON.stringify(this.state.data);
            var userData = this.state.userData;
            for (let i = 0; i < selected.length; i++) {
                var data = JSON.parse(t);
                for (let j = 0; j < data.length; j++) {
                    data[j].value = selected[i][data[j].name];
                }
                userData.push(data);
            }
            // console.log("data",data); 
            // console.log(selected); 
            
        }
        if(!this.props.questionState.info && this.props.question.guidance !== '') {
            this.toggleInfoModal(true, this.props.question.guidance);
        }

        if(this.props.questionState.multiple) {
            console.log("multiple");
            this.updateOptions();
        }

    }

    toggleTableModal(visibility) {
        this.setState({ showTableModal: visibility });
    }

    updateOptions() {        
        var data = this.state.data
        var options = [];
        // console.log("mount");
        for (let i = 0; i < data.length; i++) {
            if (data[i].question_id === this.props.question.id) {
                var option = {
                    option: data[i].name,
                    guidance: data[i].guidance === "" ? false : data[i].guidance,
                    isChecked: false,
                    inputType: data[i].answer_type == 1 ? constants.BUTTON : constants.GUIDANCE, // not change to === here. type not strict
                    next: "Q0"
                };
                options.push(option);
            }
        }
        this.setState({ options: options });
    }

    saveUserData() {
        var userData = this.state.userData;
        var processedUserData = [];
        console.log(userData);
        for(let i = 0; i < userData.length; i++) {
            var row = {};
            for(let j = 0; j < userData[i].length; j++) {
                var key = userData[i][j].name;
                var value = userData[i][j].value;
                var type = userData[i][j].type;
                if(type === 'number'){
                    row[key] = Number(value);
                } else {
                    row[key] = value;
                }
                
            }
            processedUserData.push(row);
        }
        this.props.onClick(processedUserData)
    }

    submit(response) {
        console.log("submit");
        //console.log(data);
        this.toggleTableModal(false);
        var userData = this.state.userData;
        userData.push(response);
        this.setState({userData: userData});
        console.log(userData);
    }

    removeItem(index) {
        var userData = this.state.userData;
        userData.splice(index, 1);
        this.setState({userData: userData});
    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <button className="btn btn-sm btn-primary" onClick={() => this.toggleTableModal(true)}>Add</button>
                    <br/><br/>
                    <div className="table-responsive">
                        <table className="table table-condensed table-bordered table-responsive table-hover table-striped">
                            <thead>
                                <tr>
                                {this.state.data.map((value, index) => {
                                            return (<th key={index}>{value.name}</th>)
                                        })}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.userData.map((row, i) => {
                                            return (<tr key={i}>
                                                {row.map((col, j) => {
                                                return (<td key={j}>{col.value}</td>)
                                                })}
                                                <td><button className="btn btn-sm btn-primary" onClick={() => this.removeItem(i)}>Remove</button></td>
                                            </tr>)
                                        })}
                                
                            </tbody>
                        </table>
                    </div>
                {this.props.questionState.multiple ? <div>
                    {this.state.options.map((value, index) => {
                        return (<span key={index}> {value.inputType === constants.BUTTON ?
                            <button className="btn btn-primary" onClick={() => this.onClick(index)}>{value.option}</button> :
                            <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, value.guidance)}><i className="glyphicon glyphicon-info-sign"></i></button>
                            }
                            {' '}</span>)
                    })}
                </div> : <button className="btn btn-primary" disabled={this.state.userData.length === 0} onClick={() => this.saveUserData()}>Next</button> }
                {' '}{this.props.questionState.info? <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, this.props.question.guidance)}>Info</button> : null}
                {this.state.showTableModal ? <TableModal data={this.state.data} visibility={this.state.showTableModal} onSubmit={(response) => this.submit(response)} onCancel={() => this.toggleTableModal(false)} /> : null}
                {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}