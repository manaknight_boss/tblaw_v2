import React, { Component } from 'react';
import constants from './asserts/constants';
import StartQuestionaire from './StartQuestionaire';
import Axios from 'axios';
import QuestionWithOneButton from './QuestionWithOneButton';
import QuestionWithInfo from './QuestionWithInfo';
import QuestionWithBlank from './QuestionWithBlank';
import QuestionWithTable from './QuestionWithTable';
import QuestionWithMixTable from './QuestionWithMixTable';
import ShowResult from './ShowResult';
import QuestionWithMultipleOptions from './QuestionWithMultipleOptions';
import QuestionWithButtons from './QuestionWithButtons';
import QuestionWithSplitTable from './QuestionWithSplitTable';
export default class Questionaire extends Component {

    constructor(props) {
        super(props)
        const { handle } = this.props.match.params
        this.state = {
            id: handle,
            data: null,
            isDataLoaded: false,
            isStarted: false,
            questionStates: [],
            currentQuestion: -1,
            output: {
                enable: true,
                values: []
            },
            currentState: 'Start',
            slug: '',
            blankAnswers: [],
            tableData: null,
            isEnded: false,
            isTempEnd: false
        }
    }

    updateBlankAnswer(question, value) {
        var blankAnswers = this.state.blankAnswers;
        var isFound = false;
        for (let i = 0; i < blankAnswers.length; i++) {
            if (blankAnswers[i].id === question.id) {
                blankAnswers[i].answer = value;
                isFound = true;
            }
        }
        if (!isFound) {
            var blankAnswer = {
                id: question.id,
                answer: value
            };
            blankAnswers.push(blankAnswer);
        }
        this.setState({ blankAnswers: blankAnswers });
    }

    isAnySkippedQuestionLeft() {
        const states = this.state.questionStates;
        for (let i = 0; i < states.length; i++) {
            if (states[i].has_skipped === true) {
                return true;
            }
        }
        return false;
    }

    next(currentType, userData) {
        var isTempEnd = this.state.isTempEnd;
        if (this.state.currentQuestion === this.state.questionStates.length - 1) {
            isTempEnd = true;
            this.setState({ isTempEnd: true });
            if (!this.isAnySkippedQuestionLeft()) {
                this.setState({ isEnded: true });
                return;
            }

        }
        var value = 0;
        if (currentType === constants.TYPE_BLANK) {
            var blankAnswers = this.state.blankAnswers;
            for (let i = 0; i < blankAnswers.length; i++) {
                if (this.getQuestion(this.state.questionStates[this.state.currentQuestion]).id === blankAnswers[i].id) {
                    value = blankAnswers[i].answer;
                    break;
                }
            }

        } else if (currentType === constants.TYPE_TABLE || currentType === constants.TYPE_MIX_TABLE) {
            value = userData;
            if (currentType !== constants.TYPE_MIX_TABLE) {
                // var tableData = this.state.tableData;
                // for (let i = 0; i < tableData.length; i++) {
                //     if (this.getQuestion(this.state.questionStates[this.state.currentQuestion]).id === tableData[i].id) {
                //         value = tableData[i];
                //         break;
                //     }
                // }
                this.setState({ tableData: userData });
            }
        } else if (currentType === constants.TYPE_MULTIPLE || currentType === constants.TYPE_BUTTONS) {
            value = userData;
        } else {
            value = Number(this.getQuestion(this.state.questionStates[this.state.currentQuestion]).value);
        }
        this.getQuestion(this.state.questionStates[this.state.currentQuestion]).value = value;
        this.updateOutput(this.state.currentState, value);
        this.removeSkipStatus();
        this.updateAnswerStatus();
        if (isTempEnd) {
            console.log("temp end");
            const nextSkippedQuestionIndex = this.getNextSkippedQuestionIndex();
            console.log("index", nextSkippedQuestionIndex);
            if (nextSkippedQuestionIndex === -1) {
                this.setState({ isEnded: true });
                return;
            } else {
                this.setState({ currentQuestion: nextSkippedQuestionIndex }, () => this.updateCurrentState()
                );
            }
        } else {
            this.setState(prevState => {
                return { currentQuestion: prevState.currentQuestion + 1 }
            }, () => this.updateCurrentState()
            );
        }
    }

    getNextSkippedQuestionIndex() {
        const states = this.state.questionStates;
        for (let i = 0; i < states.length; i++) {
            if (states[i].has_skipped === true) {
                return i;
            }
        }
        return -1;
    }

    skip() {
        // if(this.state.currentQuestion === this.state.questionStates.length - 1) {
        //     this.setState({isEnded: true});
        //     return;
        // }
        var isTempEnd = this.state.isTempEnd;
        this.updateSkippedStatus();
        if (isTempEnd) {
            // console.log("temp end");
            const nextSkippedQuestionIndex = this.getNextSkippedQuestionIndex();
            // console.log("index", nextSkippedQuestionIndex);
            if (nextSkippedQuestionIndex === -1) {
                this.setState({ isEnded: true });
                return;
            } else {
                this.setState({ currentQuestion: nextSkippedQuestionIndex }, () => this.updateCurrentState()
                );
            }
        } else {
            this.setState(prevState => {
                return { currentQuestion: prevState.currentQuestion + 1 }
            }, () => this.updateCurrentState()
            );
        }
    }

    updateData(questionState) {
        var data = this.state.data
        data.states[questionState.name] = questionState;

    }

    saveData() {
        const url = constants.baseUrl + constants.endpoint.SAVE + this.state.slug;
        console.log("Api == " + url);
        let data = { "fsm": JSON.stringify(this.state.data), "state": this.state.currentState, "output": JSON.stringify(this.state.output) };
        Axios.post(url, data, { headers: { 'Content-Type': 'application/json' } }).then(response => {
            console.log(response.data)
        })
    }

    getQuestionBody(type, question, questionState) {
        switch (type) {
            case constants.TYPE_ONE_BUTTON:
                return <QuestionWithOneButton question={question} onClick={() => this.next(type)} onSkip={() => this.skip()} questionState={questionState} skippable={questionState.skipable && !questionState.answered} />;
            case constants.TYPE_INFO_BUTTON:
                return <QuestionWithInfo question={question} onClick={() => this.next(type)} info={question.guidance} questionState={questionState}/>;
            case constants.TYPE_BLANK:
                return <QuestionWithBlank question={question} onClick={() => this.next(type)} onChange={(value) => this.updateBlankAnswer(question, value)} text={questionState.answered ? question.value : ''} questionState={questionState}/>;
            case constants.TYPE_TABLE:
                return <QuestionWithTable question={question} onClick={(userData) => this.next(type, userData)} data={question.data} selected={questionState.answered ? question.value : ''} questionState={questionState}/>;
            case constants.TYPE_MIX_TABLE:
                return <QuestionWithMixTable question={question} onClick={(userData) => this.next(type, userData)} data={question.data} selected={questionState.answered ? question.value : ''} userData={this.state.tableData} questionState={questionState}/>;
            case constants.TYPE_SPLIT_TABLE:
                return <QuestionWithSplitTable question={question} onClick={(userData) => this.next(type, userData)} data={question.data} selected={questionState.answered ? question.value : ''} userData={this.state.tableData} questionState={questionState}/>;
            case constants.TYPE_MULTIPLE:
                return <QuestionWithMultipleOptions question={question} otherRequired={questionState.blank} onClick={(userData) => this.next(type, userData)} data={this.state.data.answer} multiAnswer={questionState.next_action[0].type === 'multi_answer'} selected={questionState.answered ? question.value : ''} questionState={questionState}/>;
            case constants.TYPE_BUTTONS:
                //console.log("Question", question);
                return <QuestionWithButtons question={question} onClick={(userData) => this.next(type, userData)} data={this.state.data.answer} questionState={questionState}/>;
            default:
                return null;
        }
    }

    getQuestionType() {
        // if (this.state.questionStates[this.state.currentQuestion].info) {
        //     return constants.TYPE_INFO_BUTTON;
        // } else 
        if (this.state.questionStates[this.state.currentQuestion].blank && !this.state.questionStates[this.state.currentQuestion].multiple) {
            return constants.TYPE_BLANK;
        } else if (this.state.questionStates[this.state.currentQuestion].multiple && this.state.questionStates[this.state.currentQuestion].type === 0 && this.state.questionStates[this.state.currentQuestion].haveOptions && (this.state.questionStates[this.state.currentQuestion].next_action[0].type === constants.BUTTON || this.state.questionStates[this.state.currentQuestion].next_action[0].type === constants.GUIDANCE)) {
            return constants.TYPE_BUTTONS;
        } else if (this.state.questionStates[this.state.currentQuestion].multiple && this.state.questionStates[this.state.currentQuestion].type === 0 && this.state.questionStates[this.state.currentQuestion].haveOptions) {
            return constants.TYPE_MULTIPLE;
        } else if (this.state.questionStates[this.state.currentQuestion].type === 1) {
            return constants.TYPE_TABLE;
        } else if (this.state.questionStates[this.state.currentQuestion].type === 2) {
            return constants.TYPE_MIX_TABLE;
        } else if( this.state.questionStates[this.state.currentQuestion].type == 4) {
            return constants.TYPE_SPLIT_TABLE;
        } else {
            return constants.TYPE_ONE_BUTTON;
        }
    }

    //function which is called the first time the component loads
    componentDidMount() {
        this.getData();
    }

    //Function to get the question Data from json
    getData() {
        const uri = this.props.location.search;
        const obj = this.convertToObject(uri);
        console.log(uri);
        console.log(obj);
        const url = constants.baseUrl + constants.endpoint.NEW + this.state.id;
        console.log("Api == " + url);
        Axios.get(url, { headers: { 'Content-Type': 'application/json' } }).then(response => {
            console.log(response.data.data.fsm)
            this.setState({ slug: response.data.data.slug });
            this.setState({ data: JSON.parse(response.data.data.fsm) });
            this.extractStates();
            this.setState({ isDataLoaded: true });
            this.props.history.push('?q=' + response.data.data.slug);
        })
    }

    convertToObject(url) {
        const arr = url.slice(1).split(/&|=/);
        let params = {};

        for (let i = 0; i < arr.length; i += 2) {
            const key = arr[i], value = arr[i + 1];
            params[key] = value;
        }
        return params;
    }

    getQuestion(currentQuestion) {
        for (let i = 0; i < this.state.data.question.length; i++) {
            var question = this.state.data.question[i];
            if (question.id === currentQuestion.question_id && question.flowchart_name === currentQuestion.name) {
                return question;
            }
        }
    }

    extractStates() {
        var states = [];
        for (let i = 0; i < this.state.data.question_name.length; i++) {
            var questionName = this.state.data.question_name[i];
            var state = this.state.data.states[questionName];
            var answers = this.state.data.answer;
            var haveOptions = false;
            for (let j = 0; j < answers.length; j++) {
                if (answers[j].question_id === state.question_id) {
                    haveOptions = true;
                    break;
                }
            }
            if (haveOptions) {
                state.haveOptions = true;
            } else {
                state.haveOptions = false;
            }
            states.push(this.state.data.states[questionName]);
        }
        this.setState({ questionStates: states });
        //console.info("length")
        console.info("data", states);
    }

    startQuiz() {
        this.setState({ isStarted: true });
        this.updateOutput("Start", 0);
        this.setState({ currentQuestion: 0 }, () => {
            this.updateCurrentState();
        }
        );
    }

    updateCurrentState() {
        // this.updateSelectedStatus();
        this.updateVisitStatus();
        this.setState({ currentState: this.state.questionStates[this.state.currentQuestion].name }, () => {
            this.saveData();
        });
    }

    questionClick(index, enabled) {
        if (enabled && !this.state.isEnded) {
            this.setState({ currentQuestion: index }, () => {
                this.updateCurrentState();
            });
        }
    }

    updateVisitStatus() {
        const index = this.state.currentQuestion;
        var questionState = this.state.questionStates[index];
        questionState.visited = true;
        this.updateData(questionState);
    }

    updateAnswerStatus() {
        const index = this.state.currentQuestion;
        var questionState = this.state.questionStates[index];
        questionState.answered = true;
        this.updateData(questionState);
    }

    updateSkippedStatus() {
        const index = this.state.currentQuestion;
        var questionState = this.state.questionStates[index];
        questionState.has_skipped = true;
        this.updateData(questionState);
    }

    removeSkipStatus() {
        const index = this.state.currentQuestion;
        var questionState = this.state.questionStates[index];
        questionState.has_skipped = false;
        this.updateData(questionState);
    }

    updateOutput(state, value) {
        var output = this.state.output.values;
        var index = -1;
        for (let i = 0; i < output.length; i++) {
            if (state === output[i].state) {
                index = i;
                break;
            }
        }
        let newOutput = { "state": state, "value": value }
        if (index === -1) {
            output.push(newOutput);
        } else {
            output[index] = newOutput;
        }
    }

    getQuestionClass(question, selected) {
        if (!question.visited) {
            return "not-clickable-question";
        } else if (selected) {
            return "active-question";
        } else {
            return "name";
        }
    }

    render() {
        return (
            <div className="questionaire-page">
                <header>
                    <nav className="navbar navbar-inverse">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">
                                TBLaw
                        </a>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav">
                                <li className="active"><a href="#"><span ng-if="guide">{this.state.id}</span></a></li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-3 col-lg-2">
                            <div className="tblaw_leftbar">
                                {this.state.isDataLoaded ?
                                    <ul className="list-group">
                                        {this.state.questionStates.map((value, index) => {
                                            if (index < this.state.questionStates.length - 1) {
                                                return (<li className="list-group-item" key={index} onClick={() => this.questionClick(index, value.visited)}>
                                                    <span className={this.getQuestionClass(value, index === this.state.currentQuestion)}>{value.name}</span>{' '}
                                                    {index === this.state.currentQuestion ? <span><i className="glyphicon glyphicon glyphicon-pencil"></i></span> : null}
                                                    {value.answered ? <span><i className="glyphicon glyphicon-ok"></i></span> : null}
                                                    {value.has_skipped && !value.answered ? <span>( Skipped )</span> : null}
                                                </li>)
                                            }
                                        })}
                                    </ul> : null}
                            </div>
                        </div>
                        <div className="col-sm-9 col-lg-10">
                            {!this.state.isDataLoaded ?
                                <h1>Loading Data</h1> : null
                            }
                            {this.state.isDataLoaded && !this.state.isStarted ?
                                <StartQuestionaire title={this.state.data.start.title} body={this.state.data.start.description} onClick={() => this.startQuiz()} /> : null
                            }
                            {this.state.isEnded ?
                                <ShowResult result={this.state.data} /> : null
                            }
                            {this.state.isStarted && !this.state.isEnded ?
                                this.getQuestionBody(this.getQuestionType(), this.getQuestion(this.state.questionStates[this.state.currentQuestion]), this.state.questionStates[this.state.currentQuestion]) : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
