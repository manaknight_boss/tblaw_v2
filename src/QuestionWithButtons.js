import React, { Component } from 'react';
import GuidanceModal from './GuidanceModal';
import constants from './asserts/constants';

export default class QuestionWithButtons extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            options: [],
            question: null,
            showGuidanceModal: false,
            guidanceModalBody: ''
        }

        //console.log("const");
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    componentDidMount() {
        this.update()
    }

    updateOptions() {        
        var data = this.state.data
        var options = [];
        // console.log("mount");
        for (let i = 0; i < data.length; i++) {
            if (data[i].question_id === this.state.question.id) {
                var option = {
                    option: data[i].name,
                    guidance: data[i].guidance === "" ? false : data[i].guidance,
                    isChecked: false,
                    inputType: data[i].answer_type == 1 ? constants.BUTTON : constants.GUIDANCE, // not change to === here. type not strict
                    next: "Q0"
                };
                options.push(option);
            }
        }
        this.setState({ options: options });
    }

    update() {
        this.setState({question: this.props.question}, () => this.updateOptions());
    }

    componentDidUpdate(prevProps) {
        if (prevProps.question.id !== this.props.question.id) {
            this.update();
        }
    }

    onClick(selected) {
        var options = this.state.options;
        for (let i = 0; i < options.length; i++) {
            if(selected === i) {
                options[i].isChecked = true;
            }
        }
        this.props.onClick(options);
    }

    render() {
        return (
            <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                <div>
                    {this.state.options.map((value, index) => {
                        return (<span key={index}> {value.inputType === constants.BUTTON ?
                            <button className="btn btn-primary" onClick={() => this.onClick(index)}>{value.option}</button> :
                            <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, value.guidance)}><i className="glyphicon glyphicon-info-sign"></i></button>
                            }
                            {' '}</span>)
                    })}
                </div>
                {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}
