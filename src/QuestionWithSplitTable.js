import React, { Component } from 'react';
import TableModal from './TableModal';
import GuidanceModal from './GuidanceModal';

export default class QuestionWithSplitTable extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            userData: [],
            showTableModal: false,
            currentTable: -1,
            currentCol: -1,
            currentData: [],
            complete: false,
            guidanceModalBody: ''
        }
    }

    toggleInfoModal(visibility, body) {
        this.setState({ showGuidanceModal: visibility });
        this.setState({ guidanceModalBody: body });
    }

    componentDidMount() {
        var userInput = this.props.userData;
        var t = JSON.stringify(this.state.data);
            var userData = this.state.userData;
            for (let i = 0; i < userInput.length; i++) {
                var data = JSON.parse(t);
                for (let j = 0; j < data.length; j++) {
                    if(userInput[i][data[j].name] == null) {
                        data[j].value = '';
                    } else {
                        data[j].value = userInput[i][data[j].name];
                    }
                    
                }
                var rows = [];
                rows.push(data);
                userData.push(rows);
            }
            if(!this.props.questionState.info && this.props.question.guidance !== '') {
                this.toggleInfoModal(true, this.props.question.guidance);
            }
    }

    toggleTableModal(visibility) {
        this.setState({ showTableModal: visibility });
    }

    submit(response) {
        // console.log("submit");
        // console.log(response);
        this.toggleTableModal(false);
        var t = JSON.stringify(this.state.userData);
        var userData = JSON.parse(t);
        //var userData = this.state.userData;
        var row = userData[this.state.currentTable][0];
        var count = 0;
        for(let i = 0; i < row.length; i++) {
            if(row[i].state === "new") {
                // console.log("new");
                row[i].value = response[count++].value;
            }
            // console.log();
        }
        userData[this.state.currentTable].push(row);
        this.setState({userData: userData}, () => this.checkIfComplete());
        
    }

    checkIfComplete() {
        var userData = this.state.userData;
        for(let i = 0;i < userData.length; i++) {
            //console.log("length",userData[i].length);
            if(userData[i].length === 1) {
                return;
            }
            // for(let j = 0; j < userData[i].length; j++) {
            //     if(userData[i][j].value === '') {
            //         return;
            //     }
            // }
        }
        this.setState({complete: true});
    }

    removeValue(table, row) {
        var userData = this.state.userData;
        userData[table].splice(row, 1);
        this.setState({userData: userData});
        this.setState({complete: false});
    }

    addValue(table) {
        var userData = this.state.userData;
        var currentData = this.state.currentData;
        currentData = [];
        for(let i = 0; i < userData[table][0].length; i++) {
            if(userData[table][0][i].state === "new") {
                currentData.push(userData[table][0][i]);
            }
        } 
        this.setState({currentData: currentData});
        this.setState({currentTable: table});
        this.toggleTableModal(true);
    }

    saveUserData() {
        var userData = this.state.userData;
        var processedUserData = [];
        console.log(userData);
        for(let i = 0; i < userData.length; i++) {
            var row = {};
            for(let j = 0; j < userData[i].length; j++) {
                var key = userData[i][j].name;
                var value = userData[i][j].value;
                var type = userData[i][j].type;
                if(type === 'number'){
                    row[key] = Number(value);
                } else {
                    row[key] = value;
                }
            }
            processedUserData.push(row);
        }
        this.props.onClick(processedUserData)
    }

    render() {
        return (
            <div>
                <div>
                <h1>{this.props.question.name}</h1>
                <p>{this.props.question.body}</p>
                    <br /><br />
                    {this.state.userData.map((table, i) => {
                    return (
                        <div key={i} >
                            <button className="btn btn-sm btn-primary" onClick={() => this.addValue(i)}>Add</button>
                    <br/><br/>
                    List for {table[0][0].value}
                    <br/><br/>
                    <div className="table-responsive">
                        <table className="table table-condensed table-bordered table-responsive table-hover table-striped">
                            <thead>
                                <tr>
                                    {this.state.data.map((value, index) => {
                                        return (<th key={index}>{value.name}</th>)
                                    })}
                                </tr>
                            </thead>
                            <tbody>
                            {table.map((row, j) => {
                                if(j > 0) {
                                        return (<tr key={j}>
                                            {row.map((col, k) => {
                                                return (<td key={k}>{col.value}{' '}
                                                {col.value !== '' && col.state === "new" ?
                                                <button className="btn btn-sm btn-primary" onClick={() => this.removeValue(i, j)}>Remove</button>
                                                : null}
                                                </td>)
                                            })}
                                        </tr>)
                                } else {
                                    return null;
                                }
                                    })}   
                            </tbody>
                        </table>
                    </div>
                    </div>)})}
                    <button className="btn btn-primary" disabled={!this.state.complete} onClick={() => this.saveUserData()}>Next</button>
                    {' '}{this.props.questionState.info? <button className="btn btn-info" onClick={() => this.toggleInfoModal(true, this.props.question.guidance)}>Info</button> : null}
                </div>
                {/* <div>
                    Sorry, you can't answer this question until you fill in the question before question.
                    </div> */}
                    {this.state.showTableModal ? <TableModal data={this.state.currentData} visibility={this.state.showTableModal} onSubmit={(response) => this.submit(response)} onCancel={() => this.toggleTableModal(false)} /> : null}
                    {this.state.showGuidanceModal ? <GuidanceModal body={this.state.guidanceModalBody} visibility={this.state.showGuidanceModal} onClick={() => this.toggleInfoModal(false)} /> : null}
            </div>
        )
    }
}
